package com.samuelHCB.photoGalleryApplication.controller;

import com.samuelHCB.photoGalleryApplication.dto.AlbumDto;
import com.samuelHCB.photoGalleryApplication.entity.Album;
import com.samuelHCB.photoGalleryApplication.entity.Picture;
import com.samuelHCB.photoGalleryApplication.service.PictureService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/photo")
@Slf4j
public record PictureGalleryController(PictureService pictureService) {

    @PostMapping("/createalbum")
    public ResponseEntity<String> createAlbum(@RequestBody AlbumDto albumDto){
        log.info("CREATING ALBUM {}",albumDto.name());
        pictureService.createAlbum(albumDto);
        return new ResponseEntity<>("Album Created", HttpStatus.CREATED);
    }
    @PostMapping("/uploadpicture")
    public ResponseEntity<String> uploadPicture ( @RequestParam("picture") MultipartFile picture){
        log.info("UPLOADING PICTURE {}",picture.getOriginalFilename());
        pictureService.addPicture(picture);
        return new ResponseEntity<>("UPLOAD SUCCESS",HttpStatus.CREATED);
    }
    @PostMapping("/uploadToAlbum")
    public ResponseEntity<String> uploadToAlbum(@RequestParam("albumName") String albumName,@RequestParam("picture")
                                                MultipartFile picture){
        log.info("UPLOADING TO ALBUM {}",albumName);
        pictureService.savePictureToAlbum(albumName,picture);
        return new ResponseEntity<>("UPLOAD SUCCESS",HttpStatus.CREATED);
    }
    @PostMapping("/addPictureToAlbum/{albumName}/{pictureName}")
    public ResponseEntity<String> addPictureToAlbum(@PathVariable String albumName,@PathVariable String pictureName){
        log.info("ADDING PICTURE {} TO ALBUM {}",pictureName,albumName );
        pictureService.addPictureToAlbum(albumName,pictureName);
        return new ResponseEntity<>("ADDED SUCCESS",HttpStatus.CREATED);
    }
    @GetMapping("/getallalbums")
    public ResponseEntity<List<Album>> getAllAlbums (){
        log.info("GETTING ALL ALBUMS");
        return new ResponseEntity<>(pictureService.getAllAlbum(),HttpStatus.FOUND);
    }
    @GetMapping("/getallpictures")
    public ResponseEntity<List<Picture>> getAllPictures(){
        log.info("GETTING ALL PICTURES");
        return  new ResponseEntity<>(pictureService.getAllPicture(),HttpStatus.FOUND);
    }
    @GetMapping("/getpicturesinalbum/{albumName}")
    public ResponseEntity<Set<Picture>> getPicturesInAlbum (@PathVariable String albumName){
        log.info("GETTING ALL PICTURES IN {}", albumName);
        return new ResponseEntity<>(pictureService.getPictureFromAlbum(albumName),HttpStatus.FOUND);

    }
    @PatchMapping("/updatepicture")
    ResponseEntity<Picture> updatePicture (@RequestParam("pictureName") String pictureName,@RequestParam("picture") MultipartFile picture){
        log.info("UPDATING PICTURE {}",pictureName);
        return  new ResponseEntity<>(pictureService.updatePicture(pictureName,picture),HttpStatus.CREATED);
    }
    @PatchMapping("/updatePictureInAlbum")
    public ResponseEntity<Album> updatePictureInAlbum(@RequestParam("albumName") String albumName,
                                                      @RequestParam("pictureName") String pictureName,
                                                      @RequestParam("picture")MultipartFile picture){
        log.info("UPDATING PICTURE IN ALBUM {}",albumName);
        return new ResponseEntity<>(pictureService.updatePictureFromAlbum(albumName,pictureName,picture),HttpStatus.ACCEPTED);
    }
}
