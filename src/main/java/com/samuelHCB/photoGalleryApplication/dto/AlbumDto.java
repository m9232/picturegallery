package com.samuelHCB.photoGalleryApplication.dto;


import lombok.Getter;

import java.io.Serializable;


public record AlbumDto(String name, String description) implements Serializable {
    @Override
    public String name() {
        return name;
    }

    @Override
    public String description() {
        return description;
    }
}
