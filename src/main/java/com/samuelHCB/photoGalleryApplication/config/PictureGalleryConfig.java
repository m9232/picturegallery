package com.samuelHCB.photoGalleryApplication.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PictureGalleryConfig {
    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }
}
